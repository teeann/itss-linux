#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "helper.h"

int generateNumToSeek(int num) {
    int count = 0;
    while(num != 0) {
        num /= 10;
        ++count;
    }
    return count+1;
}

void write_history(char *syshang, char *sysmay, char *soluong) {
    //Ghi vao file history
    printf("Ban %s o may %s, con lai %s\n",syshang, sysmay, soluong );
    FILE *history;
    time_t t = time(NULL);
    struct tm tm;
    history = fopen("history.txt","a");
    tm = *localtime(&t);
    fprintf(history,"%s\t%s\t%d-%d-%d %d:%d:%d\n",sysmay,syshang,  tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    fclose(history);
    printf("Da in xong\n");
}

void bind_socket(int *socket_server, struct sockaddr_in *server, int port) {
    int opt = 1;
    if ((*socket_server = socket(AF_INET, SOCK_STREAM, 0)) == 0) { 
        perror("socket failed"); 
        exit(EXIT_FAILURE); 
    }
    // Forcefully attaching socket to the port 8080 
    if (setsockopt(*socket_server, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) { 
        perror("setsockopt"); 
        exit(EXIT_FAILURE); 
    } 
    server->sin_family = AF_INET;
    server->sin_addr.s_addr = INADDR_ANY;
    server->sin_port = htons(port);

    if (bind(*socket_server, (struct sockaddr *) server, sizeof(*server)) < 0) {
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    }

    if (listen(*socket_server, 3) < 0) {
        perror("listen"); 
        exit(EXIT_FAILURE); 
    }
}

void parse_message(int sock, char *message_client, char *ten_may, char *ten_hang) {
    ten_may = strtok(message_client,";");
    ten_hang = strtok(NULL,"\n");
}

void error(char *msg) {
    perror(msg);
}

void child_handle_socket(int socket_client) {
    char thanhcong[MSG_SIZE] = "success", thatbai[MSG_SIZE] ="failed";
    char message_client[MSG_SIZE];

    printf("inside fork child process\n");
    while(1) {
        int xacnhan = 0;
        FILE *f;
        int num_seek,num_seek_last;
        char xau[1000], may[1000], hang[1000],soluong[1000]="0";

        Inventory inventory = receive_inventory(socket_client);
        /* printf("receive from client: %s\n", message_client); */
        /* parse_message(socket_client, message_client, ten_may, ten_hang); */

        /* printf("ten may: %s\n", inventory.ten_may); */
        /* printf("ten hang: %s\n", inventory.ten_hang); */
        f = fopen("inventory.txt","r+");

        while(!feof(f)) {
            fgets(xau, 1000, f);
            if(feof(f)) {
                break;
            }
            char *sys_may = strtok(xau,";");
            char *sys_hang = strtok(NULL,";");
            int  num = atoi(strtok(NULL,"")) - 1;
            char currnum[1000];

            /* printf("sys_may: %s - sys_hang: %s\n", sys_may, sys_hang); */
            if(strcmp(inventory.ten_may, sys_may) != 0 || strcmp(inventory.ten_hang, sys_hang) !=0 ) {
                continue;
            }

            //chuyen hang den may
            if(num<3) {
                char client_message[MSG_SIZE];
                printf("Delivery....\n");
                sprintf(client_message,"increase 10 %s in %s\n",sys_hang,sys_may);
                send(socket_client, client_message, MSG_SIZE, 0);
                sleep(5);

                num+=10;
                strcpy(soluong,"");

                printf("Done!......\n");
            }
            if(num>=10) {
                strcpy(soluong,"");
            }

            sprintf(currnum,"%d",num);
            strcat(soluong,currnum);
            fseek(f,-3,SEEK_CUR);
            fputs(soluong,f);

            xacnhan = 1;
            write(socket_client, thanhcong, MSG_SIZE);
            write_history(sys_hang, sys_may, soluong);
            break;
            printf("\n\n\n\n");
        }
        fclose(f);
        if(xacnhan == 0) {
            write(socket_client,thatbai, MSG_SIZE);
            break;
        }
    }
    close(socket_client);
    /* exit(0); */
}

int main(int argc, char const *argv[])
{
    int pid;
    int socket_server;
    struct sockaddr_in server, client;
    socklen_t addr_size;

    bind_socket(&socket_server, &server, 8888);
    printf("listening on 127.0.0.1:8888\n");

    while(1) {
        int socket_client = accept(socket_server, (struct sockaddr *) &client, (socklen_t*) &addr_size);
        if (socket_client < 0) {
            error("Error in accept socket: "); 
            exit(EXIT_FAILURE); 
        }
        printf("receive new connection\n");
        pid = fork();
        if(pid<0) {
            error("fork failed");
            close(socket_client);
            continue;
        } else if(pid>0){
            continue;
        }
        child_handle_socket(socket_client);    
    }
    
    return 0;
}
