#ifndef HELPER_H
#define HELPER_H

#define MSG_SIZE 100

typedef struct Inventory {
    char ten_may[100];
    char ten_hang[100];
    int soluong;
} Inventory;

Inventory create_inventory(char *ten_may, char *ten_hang, int soluong);
void send_inventory(int sock, Inventory inventory);
Inventory receive_inventory(int sock);

#endif /* HELPER_H */
