#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include "helper.h"

Inventory create_inventory(char *ten_may, char *ten_hang, int soluong) {
    Inventory inventory;

    strcpy(inventory.ten_hang, ten_hang);
    strcpy(inventory.ten_may, ten_may);
    inventory.soluong = soluong;
    return inventory;
}

void send_inventory(int sock, Inventory inventory) {
    send(sock, &inventory, sizeof(inventory), 0);
}

Inventory receive_inventory(int sock) {
    Inventory inventory;
    recv(sock, &inventory, sizeof(Inventory), 0);
    return inventory;
}
