#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>

#include "helper.h"

int create_sock(char *server_ip, int server_port) {
    int sock;
    struct sockaddr_in server;

    //tao socket ket noi den server
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("Create socket error");
        return -1;
    }

    server.sin_addr.s_addr = inet_addr(server_ip);
    server.sin_family = AF_INET;
    server.sin_port = htons(server_port);
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
        printf("\nConnection Failed \n"); 
        return -1; 
    }
    return sock;
}

Inventory get_input() {
    char ten_hang[50], ten_may[50];

    printf("Nhap ten may: ");
    scanf("%s",ten_may);
    fflush(stdin);
    printf("Nhap do vat muon mua: ");
    scanf("%s",ten_hang);
    fflush(stdin);
    return create_inventory(ten_may, ten_hang, 0);
}

void receive(char *reply, int sock) {
    recv(sock, reply, MSG_SIZE, 0);
}

int main(int argc, char const *argv[]) {
    char *ip = "127.0.0.1";
    int port = 8888;
    int sock = create_sock(ip, port);
    char message[MSG_SIZE];
    char server_reply[MSG_SIZE];
    int status = 0; 

    if (sock < 0) {
        return -1; 
    }
    printf("listening on %s:%d\n", ip, port);
    while(status != 1) {
        ///nhap hang va ten may
    
        Inventory inventory = get_input();
        
        printf("sending %s-%s\n", inventory.ten_hang, inventory.ten_may);
        send_inventory(sock, inventory);
        
        receive(server_reply, sock);
        
        printf("Server reply: %s\n", server_reply);
        printf("Ban co muon thoat? An 1 de thoat\n");
        scanf("%d",&status);
        fflush(stdin);
    }
    close(sock);
    return 0;
}
